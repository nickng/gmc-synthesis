module CFSM where

import Prelude hiding (succ)
import Data.Set as S
import Data.List as L
import Data.List (sortBy)
import System.Environment
import Misc as M
import Data.Foldable as F
import Data.Map as M

type Message = String
type Id = Int
type State = String

data Dir = Send | Receive | Tau
         deriving (Eq,Ord,Show)
                  
type Channel = (Id, Id)
                  
type Action = (Dir, Channel, Message)

type LTrans = (State, Action, State)

type CFSM = (Set State, State, Set Action, Set LTrans)

type System = [CFSM]

type Diamond = Set ((State, Action), (State, Action))

type DiamondMap = Map Id Diamond

emptyCFSM :: CFSM
emptyCFSM = (S.empty, "", S.empty, S.empty)

stateNumber :: CFSM -> Int
stateNumber (sts,s0, evs, trs) = S.size sts

stateNumbers :: System -> [Int]
stateNumbers sys = L.map stateNumber sys

existSend :: Set Action -> Bool
existSend set = F.or $ S.map (\(dir,chan,msg) -> dir == Send) set

dualAction :: Action -> Action
dualAction (Send,channel,msg) = (Receive,channel,msg)
dualAction (Receive,channel,msg) = (Send,channel,msg)

-- 
-- Basic Functions (for TS constructions)
--
succ :: CFSM -> State -> Action -> State
succ (sts,s0, evs, trs) s e = head sucs
  where sucs = S.toList $ S.map (\(x,y,z) -> z) (S.filter (\(x,e',z) -> x == s && e == e') trs)
        
        
succs :: CFSM -> State -> Set (Action, State)
succs (sts,s0, evs, trs) s = 
  S.map (\(x,y,z) -> (y,z)) (S.filter (\(x,e',z) -> x == s) trs)   
        

sndActions :: CFSM -> State -> Set (Action, State)
sndActions (sts, s0, evs, trs) s = 
  S.map (\(x,y,z) -> (y,z)) (S.filter (\(x,(d, y, s'),z) -> x == s && d == Send) trs)

rcvActions :: CFSM -> State -> Set (Action, State)
rcvActions (sts, s0, evs, trs) s = 
  S.map (\(x,y,z) -> (y,z)) (S.filter (\(x,(d, y, s'),z) -> x == s && d == Receive) trs)



--
-- Diamond Computations
--
blackdiamondRel :: CFSM -> Diamond
blackdiamondRel  machine@(states, init, acts, ltrans) = S.fromList $ M.equivalenceRelation init
  where init = S.toList $ S.filter (\(x,y) -> (keepElemt x y)) prod
        setpairs = S.map (\(x,y,z) -> (x,y)) ltrans
        prod = M.cartProd setpairs setpairs
        whitediamond = whitediamondRel machine
        isWhitediamond x y = S.member (x,y) whitediamond
        equiEvt x = equivalenceClass whitediamond x
        --
        keepPair (s, a) eclass =
          F.and $ S.map (\(s',a') -> (oneStepTrans machine s s') == 
                                     (oneStepTrans machine (succ machine s a) (succ machine s' a'))) eclass
        --
        keepElemt t1 t2 = let eclass = equiEvt t1
                          in (keepPair t1 eclass) && (keepPair t2 eclass) &&  isWhitediamond t1 t2


diamond :: CFSM -> (State, Action) -> (State, Action) -> Bool
diamond machine t t' = S.member (t,t') (blackdiamondRel machine)


whitediamondRel :: CFSM -> Diamond
whitediamondRel machine@(states, init, acts, ltrans) = S.fromList $ M.equivalenceRelation init
  where init = S.toList $ S.filter (\(x,y) -> (diamondNonClosed machine x y)) prod
        setpairs = S.map (\(x,y,z) -> (x,y)) ltrans
        prod = M.cartProd setpairs setpairs
        --
        keep (s1,a1) (s2,a2) = 
           (oneStepTrans machine s1 s2) == oneStepTrans machine (succ machine s1 a1) (succ machine s2 a2)
          
diamondMap :: System -> DiamondMap
diamondMap sys = helper 0 sys M.empty
  where helper i (x:xs) acc = helper (i+1) xs (M.insert i (blackdiamondRel x) acc)
        helper _ [] acc = acc



oneStepTrans :: CFSM -> State -> State -> Set Action
oneStepTrans machine@(states, init, acts, ltrans) s s' =
  S.map (\(x,y,z) -> y) (S.filter (\(x,y,z) -> x == s && z == s') ltrans)
                 
                  
checkDiamond :: DiamondMap -> Id -> (State, Action) -> (State, Action) -> Bool
checkDiamond map id t t' = S.member (t,t') thisdia
  where thisdia = case M.lookup id map of
          Just d -> d
          Nothing -> S.empty
          

diamondNonClosed :: CFSM -> (State, Action) -> (State, Action) -> Bool
diamondNonClosed machine@(states, init, acts, ltrans) (s1, a1) (s2,a2) =
  (a1 == a2)
  &&
  (
    (s1 == s2)
    ||
    (
      not (S.null pathsrc)
      &&
      pathsrc == pathtrg
      &&
      (not $ S.member a1 pathsrc)
    )
  )
  where 
    --
    s1' = succ machine s1 a1
    s2' = succ machine s2 a2
    --
    path s s' = S.map (\(x,y,z) -> y) (S.filter (\(x,y,z) -> x == s && z == s') ltrans)
    --
    pathsrc = path s1 s2
    pathtrg = path s1' s2'

--
-- PARSING FUNCTIONS
--               
stringToAction :: Id -> [String] -> [Action]
stringToAction id (m:d:st:xs) = if d == "!"
                      then (Send,(id, (read m :: Id)),st):(stringToAction id xs)
                      else (Receive,((read m :: Id),id),st):(stringToAction id xs)
stringToAction _ [] = []

parseSystem :: Id -> [[String]] -> CFSM -> System -> System
parseSystem  i ((x:ys):xs) g@(st, s0, ev, tr) mysys = 
  case x of
    ".outputs" -> parseSystem i xs (st, "", (S.fromList (stringToAction i ys)), tr) mysys
    ".state" -> parseSystem i xs g mysys
    ".marking" -> parseSystem i xs (st, (head ys), ev, tr) mysys
    "--" -> parseSystem i xs g mysys
    ".end" ->  parseSystem (i+1) xs emptyCFSM ((st, s0, ev, tr):mysys)
    _ -> case ys of
      [m,d,ch,s2] -> parseSystem i xs (st', s0, ev', tr') mysys
        where st' = S.insert x (S.insert s2 st)
              tr' = S.insert (x, head (stringToAction i [m,d,ch]), s2) tr
              ev' = S.map (\(x,y,z) -> y) tr'
      s ->  mysys -- error ("At least one CFSM is ill-defined (cf. "++show s++")")
parseSystem i ([]:xs) g mysys =  parseSystem i xs g mysys
parseSystem _ [] _ mysys = mysys



printState :: State -> Id-> String
printState s id = rmveQuotes $ (show s)++(show id)

printAction :: Action -> String
printAction (dir, chan, msg) =  rmveQuotes $ 
  case dir of
    Send -> (printChan chan)++"!"++(show msg)
    Receive -> (printChan chan)++"?"++(show msg)
  where printChan (x,y) = (show x)++(show y)
    
    
printMachine :: CFSM -> Id -> String
printMachine (states, init, actions, trans) id =
  let sstates = S.foldr (++) "" (S.map (\x -> (printState x id)++" [label= \""++(printState x id)++"\"];\n") states)
      transi = S.foldr (++) "" ( 
        S.map (\(s, a, t) -> 
                (printState s id)
                ++ " -> "++
                (printState t id)
                ++
                " [label= \""++(printAction a)++"\"];\n"
              )
        trans)
    in sstates ++ transi


printSystem :: System -> String
printSystem list = "digraph CFSMs { \n"++(helper 0 list)++"} \n"
  where helper i (x:xs) =
          let header = "subgraph cluster_"++(show i)
                       ++" {\n"
                       ++ "label = "++(show i)++";\n"      
              footer = "}\n"
              machine = printMachine x i     
          in (header++machine++footer)++"\n"++(helper (i+1) xs)
        helper i [] = ""
              
                      
system2File :: FilePath -> System -> IO()
system2File file sys = writeToFile (file++"_machines.dot") (printSystem sys)


