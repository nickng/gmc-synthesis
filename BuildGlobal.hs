--
-- Julien Lange <j.lange@ic.ac.uk>
--
-- module BuildGlobal where


import System.Environment
import System.FilePath.Posix
import PetrifyBridge
import PetriNet
import Misc
import GlobalGraph

main :: IO ()
main =  do putStrLn "----------------- Global Graph Synthesis -----------------"
           progargs <- getArgs
           case progargs of
             (x:xs) -> do
               putStrLn "Parsing PN file..."
               pnfile <- readFile x
               let filename = "outputs/"++(takeFileName x)
               let pn = parsePetriNet (Prelude.map (\x -> words x) (lines pnfile))
               -- putStrLn $ "\n"++(show pn)
               putStrLn "Transformation 1 (One-Source Petri Net)"
               let one_pn = oneSource pn
               writeToFile (filename++"_onesourcepn.dot") (printNet one_pn)
               putStrLn "Transformation 2 (Joined Petri Net)"
               let joined_pn = (joinPred . joinPost) one_pn
               writeToFile (filename++"_finalpn.dot") (printNet joined_pn)
               --
               putStrLn "Transformation 3 (PN to Pre-Global Graph)"
               let pregg =  net2globalgraph joined_pn
               putStrLn "Transformation 4 (Pre-Global Graph to Global Graph)"
               writeToFile (filename++"_preglobal.dot") (globalGraph2String pregg)
               let gg = cleanupGG pregg
               -- putStrLn $ "Number of Vertices: " ++(show $ nodeNumber gg)
               -- putStrLn $ "Number of Transitions: " ++(show $ transNumber gg)
               writeToFile (filename++"_global.dot") (globalGraph2String gg)
               putStrLn "Global Graph Construction Done; see output folder."
               --
             [] -> do 
               print "Error: enter a path to a PN file"
               
               