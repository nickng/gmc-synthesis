module PartialOrderReduction where

import CFSM
import TS
import Data.Set as S
import Data.List as L
import Data.Maybe
import Data.List (sortBy)
import System.Environment
import Misc
import Data.Map as M
import Data.Either
import Data.Tree as T
import Data.Foldable as F

-- TEMP
import System.IO.Unsafe
import Debug.Trace




ample :: TS -> Node -> [Set Trans]
ample ts node = if allSelfLoops then [succ] else L.map snd $ L.sortBy compareList events
  where succ = tssuccsTrans ts node
        transList = S.toList $ S.map (\x -> (machines x, S.singleton x)) succ
        events =  mygroup (length transList) transList
        fgroup (m1,e1) (m2,e2) = not $ S.null (S.intersection m1 m2)
        myfold xs = L.map (\ys ->  L.foldr merge (S.empty, S.empty) ys) xs
        merge (m1,e1) (m2,e2) = (S.union m1 m2, S.union e1 e2)
        --
        mygroup :: Int -> [(Set Id, Set Trans)] -> [(Set Id, Set Trans)] 
        mygroup i xs = let newlist = myfold (L.groupBy fgroup xs)
                       in if i > (length newlist)
                          then mygroup (length newlist) newlist
                          else xs
        compareList (m1,e1) (m2,e2) = compare (S.size e1) (S.size e2)
        --
        allSelfLoops = F.and $ S.map (\(x,y,z) -> x==z) succ
        
--
-- /!\ DO A MORE CLEVER TRAVERSAL HERE?
--
checkC1 :: TS -> Node -> Set Trans -> Set Trans -> Bool
checkC1 ts current ample others = 
  F.and $ S.map (\(o,e,n) -> check n (S.delete e (S.map (\(x,y,z) -> y) (ample))) S.empty) others
  where check node evtample visited = 
          (S.member node visited)
          ||
          (S.null evtample)
          ||
          (
            let succ = S.filter (\(x,y,z) -> not $ S.member y evtample) (tssuccsTrans ts node)
                evts = S.map (\(x,y,z) -> y) succ
            in 
             (F.and $ S.map (\x -> (indep x evtample) || S.null evtample) evts)
             &&
             (F.and $ S.map (\(x,y,z) -> check z (S.delete y evtample) (S.insert node visited)) succ)
          )
        --
        indep evt set = F.and $ S.map (\x -> independent x evt) set


--
--
checkC3 :: TS -> Set Node -> Node -> Set Trans -> Bool
checkC3 ts visited current ample = 
  -- F.or $ S.map (\(x,y,z) -> not $ L.elem current $ reachableNode ts z) ample
  not $ F.or $ S.map (\(x,y,z) -> S.member (tssucc ts current y) visited) ample


chooseCandidate :: TS -> Set Node -> Node -> Set Trans
chooseCandidate ts visited current = findAmple (ample ts current) alltrans 
  -- | not $ S.null $ S.intersection (tssuccs ts current) visited = alltrans
  -- | otherwise = findAmple (ample ts current) alltrans 
  where findAmple (x:xs) all = if (x==all)
                                  || 
                                  (
                                    (checkC1 ts current x (S.difference all x))
                                    &&
                                    (checkC3 ts visited current x)
                                  )
                               then x 
                               else findAmple xs all
        findAmple [] all = all
        alltrans = tssuccsTrans ts current


--
--
reduce :: TS -> TS
reduce ts@(nodes,initnode,events,tstrans) = (newNodes, initnode, newEvents, newTrans) 
  where newNodes = S.fold S.union S.empty $ S.map (\(x,y,z) -> S.insert x (S.singleton z)) newTrans
        newEvents = S.fold S.union S.empty $ S.map (\(x,y,z) ->  S.singleton y) newTrans
        newTrans = S.fromList $ rebuild [initnode] S.empty []
        --
        rebuild (current:xs) visited acc
          | S.member current visited = rebuild xs visited acc
          | otherwise = let nextrans = S.toList $ chooseCandidate ts visited current
                            nextstates = L.map (\(x,y,z) -> z) nextrans
                        in rebuild (xs++nextstates) (S.insert current visited) (acc++nextrans)
        rebuild [] visited acc = acc