module Representability where

import Misc
import CFSM
import TS
import Data.Set as S
import Data.List as L
import HKCBridge
import Data.Foldable as F
import Control.Concurrent
import Control.Exception
import Data.Either
import WeakBisimRepr


--
-- This module handles Representability checks for part (ii) and projects files
-- to be checked with HKC, for part (i).
--

--
-- Lgge equivalence between machine and projected TS
--
repLanguage :: FilePath -> System -> TS -> IO()
repLanguage file sys ts = helper sys 0
  where helper (x:xs) i = do writeToFile (file++"_machine_"++(show i)) (cfsm2timbuk x i)
                             writeToFile (file++"_projection_"++(show i)) (cfsm2timbuk (projectTS ts i) i)
                             -- mCRL2 version (experimental)
                             writeToFile (file++"_lts_machine_"++(show i)++".fsm") (cfsm2fsm x)
                             writeToFile (file++"_lts_projection_"++(show i)++".fsm") (cfsm2fsm (projectTS ts i))
                             --
                             
                             --
                             helper xs (i+1)
        helper [] i = return ()
        
        
-- CONCURRENT VERSION
repLanguageConcurrent :: FilePath -> System -> TS -> IO()
repLanguageConcurrent file sys ts = 
  do x <- newEmptyMVar
     y <- newEmptyMVar
     helper sys 0 x y
  where 
    helper :: System -> Id -> (MVar (Either Id Id)) -> (MVar (Either Id Id)) -> IO()
    helper (x:xs) i v1 v2 = do
          forkIO $ 
            (writeToFile (file++"_machine_"++(show i)) (cfsm2timbuk x i)) `finally` putMVar v1 (Right i)
          forkIO $ 
            (writeToFile (file++"_projection_"++(show i)) (cfsm2timbuk (projectTS ts i) i)) `finally` putMVar v2 (Left i)
          helper xs (i+1) v1 v2
    helper [] idx v1 v2 = do waitChildren idx v1
                             waitChildren idx v2
        --
    waitChildren idx v
          | idx > 0 = do var <- takeMVar v
                         case var of
                           Right j -> return () -- putStrLn $ "Machine "++(show j)++" to timbuk done."
                           Left j -> return () -- putStrLn $ "Projection "++(show j)++" to timbuk done."
                         waitChildren (idx-1) v
          | idx == 0 = return ()
        



--
-- All branchings in TS
--
repBranching :: System -> TS -> Bool
repBranching sys ts@(nodes,initnode,events,tstrans) = helper sys 0 
  where helper (x:xs) i = (checkMachine x i) && (helper xs (i+1))
        helper [] _ = True
        --
        checkMachine (states, init, actions, trans) id = checkStates (S.toList states) trans id
        --
        checkStates (x:xs) trans id = (checkState x trans id) && (checkStates xs trans id)
          -- if (checkState x trans id) 
          -- then (checkStates xs trans id)
          -- else error $ (show x)++"\n"++(show trans)++"\n"++(show id)++"\n"++(show $ firstActions ts initnode id)++
          --      "\n"++(show $ S.map (\(x',y,z) -> y) (S.filter (\(x',y,z) -> x==x') trans))++
          --      "\n"++(show nodes)
        checkStates [] _ _ = True
        --
        checkState state trans id = F.or $ S.map (\x -> S.isSubsetOf setactions (setfirstactions x)) setnodes
          where setactions = S.map (\(x,y,z) -> y) (S.filter (\(x,y,z) -> x==state) trans)
                setfirstactions node = firstActions ts node id (possactions node)
                setnodes = S.filter (\x -> (x!!id) == state) nodes
                possactions node = possibleActions sys id node
