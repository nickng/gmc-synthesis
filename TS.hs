module TS where

import CFSM
import Data.Set as S
import Data.List as L
import Data.Maybe
import Data.List (sortBy)
import System.Environment
import Misc
import Data.Map as M
import Data.Either
import Data.Tree as T

-- TEMP
import System.IO.Unsafe
import Debug.Trace

type Node = [State]
type Event = (State, State, Id, Id, Message)
type Trans = (Node, Event, Node)
type TS = (Set Node, Node, Set Event, Set Trans)
type Interaction =  (Id, Id, Message)

type Bowtie = Set (Event, Event)

type ReplaceEvent = Map Event Event

--
-- This module contains function to build, project, and print TS
--


sender :: Event -> Id
sender (q,q',m1,m2,msg) = m1

receiver :: Event -> Id
receiver (q,q',m1,m2,msg) = m2

machines :: Trans -> Set Id
machines (n,(q,q',m1,m2,msg),n') = S.insert m1 (S.singleton m2)

evt2interaction :: Event -> Interaction
evt2interaction (q,q',m1,m2,msg) = (m1,m2,msg)

action2interaction :: Action -> Interaction
action2interaction (_, (m1, m2), msg) = (m1,m2,msg)

nodeNumber :: TS -> Int
nodeNumber (set, _, _ ,_ ) = S.size set

transNumber :: TS -> Int
transNumber (_, _, _ ,set) = S.size set


density :: TS -> Float 
density ts = let e = transNumber ts
                 v = nodeNumber ts
             in (fromIntegral e) / fromIntegral (v^2)
                
averageDegree :: TS -> Float
averageDegree ts = let e = transNumber ts
                       v = nodeNumber ts
                   in (fromIntegral $ 2*e) / fromIntegral (v)

representativeMap :: Bowtie -> Set Event -> ReplaceEvent
representativeMap bowtie events = helper (S.toList events) M.empty
  where helper (x:xs) acc = 
          case M.lookup x acc of
            Just y -> helper xs acc
            Nothing -> M.union (M.fromList $ L.map (\y -> (y,x)) (equiv x)) (helper xs acc)
        helper [] acc = acc
        --
        equiv e = S.toList $ S.fold S.union S.empty $
                  S.map (\(x,y) -> S.insert x (S.singleton y)) (S.filter (\(x,y) -> x == e || y ==e) bowtie)

independent:: Event -> Event -> Bool
independent (_,_,m1,m2,_) (_,_,m1',m2',_) = (m1 /= m1')  && (m1 /= m2') && (m2 /= m1')  && (m2 /= m2') 

getRepresentative :: ReplaceEvent -> Event -> Event
getRepresentative map e = case M.lookup e map of
  Just e' -> e'
  Nothing -> e

buildTS :: System -> TS
buildTS sys = 
  (S.insert initstate states, initstate, S.map refun events, diamondtrans)
  where
    (states, events, trans) = generateTrans sys  [initstate] S.empty (S.empty,S.empty,S.empty)
    initstate = L.map (\(ss, s , acts, trans) -> s) sys
    --
    diamondmap = diamondMap sys
    bowtie = bowtieRel diamondmap events  
    representMap = representativeMap bowtie events
    refun =  getRepresentative representMap -- (\x -> x) -- 
    diamondtrans = S.map  (\(n1,e,n2) -> (n1,  refun e, n2)) trans -- trans -- 
  
tssuccEvent :: TS -> Node -> Set Event
tssuccEvent ts n = S.map (\(x,y) -> x) (tssuccsPair ts n)
  -- S.map (\(x,y,z) -> (y)) (S.filter (\(x,y,z) -> x == n) tstrans)

tssucc :: TS -> Node -> Event -> Node
tssucc ts n e = head $ S.toList $ S.map (\(x,y) -> y) (S.filter (\(x,y) -> x==e) (tssuccsPair ts n))
  
--   head $ S.toList $ S.map (\(x,y,z) -> z) (S.filter (\(x,y,z) -> x == n && y == e) tstrans)

tssuccs :: TS -> Node -> Set Node
tssuccs ts n = S.map (\(x,y) -> y) (tssuccsPair ts n)
  -- S.map (\(x,y,z) -> z) (S.filter (\(x,y,z) -> x == n) tstrans)

tssuccsPair :: TS -> Node -> Set (Event, Node)
tssuccsPair (nodes,initnode,events,tstrans) n = S.map (\(x,y,z) -> (y,z)) (S.filter (\(x,y,z) -> x == n) tstrans)

tssuccsTrans :: TS -> Node -> Set Trans
tssuccsTrans (nodes,initnode,events,tstrans) n = S.filter (\(x,y,z) -> x == n) tstrans


isEmpty :: TS -> Bool
isEmpty (states, initstate, events, trans) = (S.size states < 2) && (S.null trans)

noSelfLoop :: TS -> Bool
noSelfLoop (states, initstate, events, trans) = S.null $ S.filter (\(x,y,z) -> x == z) trans


reinit :: TS -> Node -> TS
reinit (states, initstate, events, trans) n = (nodes, n, nevents, ntrans)
  where
    tmp = (states, n, events, trans)
    nodes = S.fromList $ reachableNode tmp n
    ntrans = S.filter (\(x,y,z) -> S.member x nodes && S.member z nodes) trans
    nevents = S.fold S.union S.empty $ S.map (\(x,y,z) -> S.singleton y) ntrans
--
-- Basic Synchronous Execution
--
allSnds :: System -> Node -> Set (State, Id, Action, State)
allSnds sys conf = sysh 0 sys conf S.empty
  where sysh i (m:ms) (c:cs) mset = sysh (i+1) ms cs (S.union (S.map (\(x,z) -> (c,i,x,z)) (sndActions m c)) mset)
        sysh i [] [] mset = mset
        
allRcvs :: System -> Node -> Set (State, Id, Action, State)
allRcvs sys conf = sysh 0 sys conf S.empty
  where sysh i (m:ms) (c:cs) mset = sysh (i+1) ms cs (S.union (S.map (\(x,z) -> (c,i,x,z)) (rcvActions m c)) mset)
        sysh i [] [] mset = mset
        
matchingActions :: Set (State, Id, Action, State) -> Set (State, Id, Action, State) -> Set (Event, State, State) 
matchingActions snd rcv = S.fromList $ filterMatch (findmatchList (S.toList snd) (S.toList rcv))
  where filterMatch (x:xs) = case x of
          Just evt -> evt:(filterMatch xs)
          Nothing -> (filterMatch xs)
        filterMatch [] = []
        --
        findmatchList xs ys = L.map (\x -> findmatch x ys) xs
        --
        findmatch x (y:ys) = case actionsToEvent x y of
          Just evt ->  Just evt
          Nothing -> findmatch x ys
        findmatch x [] = Nothing
          
actionsToEvent :: (State, Id, Action, State) -> (State, Id, Action, State) -> Maybe (Event, State, State)
actionsToEvent snd@(s1, m1 , (dir1, (id1,id2'), msg1), trg1) rcv@(s2, m2 , (dir2, (id1', id2), msg2), trg2) =
  if m1 == id1' && m2 == id2' && msg1 == msg2
  then Just ((s1,s2,m1,m2,msg1), trg1, trg2)
  else Nothing
       
       
       
       
consNode :: System -> Node -> (Event, State, State) -> Node
consNode sys nodes label = helper 0 sys nodes label 
  where  
    helper i sys (x:xs) evts@((s1,s2,m1,m2,msg1), trg1, trg2) =
      if i==m1
      then trg1:(helper (i+1) sys xs evts)
      else if i==m2
           then trg2:(helper (i+1)  sys xs evts)
           else x:(helper (i+1) sys xs evts)
    helper i sys [] _ = []
        
        
successors :: System -> Node -> (Set (Event, Node))
successors sys srcnode = genconf evts S.empty
  where evts = S.toList $ matchingActions (allSnds sys srcnode) (allRcvs sys srcnode)
        genconf (trip@(evt@(qs,qr,s,r,st),t1,t2):es) acc = genconf es (S.insert (evt,(consNode sys srcnode trip)) acc)
        genconf [] acc = acc
        
generateTrans :: System
                 -> [Node]
                 -> Set Node
                 -> ( Set Node, Set Event, Set Trans)
                 -> ( Set Node, Set Event, Set Trans)
generateTrans sys (n:ns) done acc@(nodes, evts, transs) =
  if S.member n done
  then generateTrans sys ns done acc
  else let pairs = successors sys n
           nnodes = S.map snd pairs
           nevts =  S.map fst pairs
           ntrans = S.map (\(x,y) -> (n,x,y)) pairs
           nacc = ((S.union nodes nnodes), (S.union nevts evts), (S.union ntrans transs))     
       in
        generateTrans sys (S.toList (S.union (S.fromList ns) nnodes)) (S.insert n done) nacc
generateTrans sys [] done acc = acc



--
-- Find all the nodes reachable from another node
--  
reachableNode :: TS -> Node -> [Node]
reachableNode ts@(nodes,initnode,events,tstrans) n0 = traverse (S.singleton n0) (S.singleton n0) [n0]
  where
    traverse border visited acc =
      if (S.null border) || (visited == nodes)
      then acc -- error $ (show n0)++"\n\n"++(show  acc)
      else
        let newborder = S.fold S.union S.empty $
                        S.map (S.filter (\ y -> not $ S.member y visited) . tssuccs ts) border
        in
         traverse newborder (S.union visited newborder) (acc++(S.toList newborder))
         


--
-- Projection
--
project :: Event -> Id -> Maybe Action
project (s1,s2,m1,m2,msg) id = 
  if m1 == id 
  then Just (Send, (m1,m2) , msg)
  else 
    if m2 == id 
    then Just (Receive, (m1,m2) , msg)
    else Nothing

-- To be used only when guaranteed that id is in e
projectUnSafe :: Event -> Id -> Action
projectUnSafe (s1,s2,m1,m2,msg) id = 
  if m1 == id 
  then (Send, (m1,m2) , msg)
  else (Receive, (m1,m2) , msg)
       
       
projectTrans :: Trans -> Id -> Maybe Action
projectTrans (n1, (s1,s2,m1,m2,msg), n2)  id = 
  if m1 == id 
  then Just (Send, (m1,m2) , msg)
  else 
    if m2 == id 
    then Just (Receive, (m1,m2) , msg)
    else Nothing
         
possibleActions :: System -> Id -> Node -> Set Action
possibleActions sys id n = let (states,init,acts,trans) = sys!!id
                               q = n!!id
                           in S.map (\(x,y,z) -> y) $ S.filter (\(x,y,z) -> x == q) trans
                
--
-- 
--
projectTS :: TS -> Id -> CFSM
projectTS ts@(nodes, initnode, events, trans) id = (states, initstate, actions, finalTrans)
  where states = S.insert initstate $  
                 S.fold (S.union) S.empty $ S.map (\(x,y,z) -> S.insert x (S.singleton z)) finalTrans
        actions = S.map (\(x,y,z) -> y ) finalTrans
        initstate = printNodeId initnode
        finalTrans = S.map (\(x,(Just y),z) ->  (printNodeId x, y, printNodeId z)) $
                     S.filter (\(x,y,z) -> isJust y) (S.union tmpltrans (addTrans (S.toList nodes) S.empty))
        tmpltrans = S.map (\(n1,e,n2) -> (n1,project e id,n2)) trans
        --
        addTrans (p:xs) acc = 
          let eclose = epsilonReachable p
              newarcs =  S.map (\(x,a,q) -> (p,a,q)) $ S.filter (\(x,a,q) -> S.member x eclose) tmpltrans 
          in addTrans xs (S.union newarcs acc)
        addTrans [] acc = acc
        --
        mysucc n =  S.map (\(x,y,z) -> (y,z)) (S.filter (\(x,y,z) -> x == n) tmpltrans)
        --
        epsilonReachable n = S.fromList $ traverse [n] S.empty [n]
        --
        traverse (n:ns) visited acc =
          if S.member n visited  
          then traverse ns visited acc
          else let sucnodes = S.toList $
                              S.map (\(l,s) -> s) $ 
                              S.filter (\(l,s) -> (isNothing $ l) && (not $ S.member s visited)) $ (mysucc n)
               in traverse (ns++sucnodes) (S.insert n visited) (acc++sucnodes)
        traverse [] visited acc = acc                                   

projectPath :: [Event] -> Id -> [Action]
projectPath (x:xs) id = case project x id of
  Just a -> a:(projectPath xs id)
  Nothing -> (projectPath xs id)
projectPath [] _ = []

projectForest :: Forest Event -> Id -> Forest Action
projectForest forest id = 
  let mix = L.map (\x -> projectTree x id) forest
      trees = rights mix -- [Tree Action] = [T1,T2,T3]
      forests = L.foldr (++) [] (lefts mix) -- [[Tree Action]] = [[T1,T2,T3],[T1,T2,T3],....]
  in trees++forests


projectTree :: Tree Event -> Id -> Either [Tree Action] (Tree Action) 
projectTree (Node e f) id =  case (project e id) of
  Just act -> Right (Node act (projectForest f id))
  Nothing -> Left ((projectForest f id))



nextStates :: Set (State, Maybe Action, State) -> State -> Set (Action, State)
nextStates trans q = helper (S.singleton q) S.empty (succs q)
  where helper visited acc ((label,q'):xs) = 
          case label of
            Just a -> helper visited (S.insert (a,q') acc) xs
            Nothing -> S.union
                       (
                         if S.member q' visited
                         then S.empty
                         else (helper (S.insert q' visited) acc (succs q')) 
                       )
                       (helper visited acc xs)
        helper visited acc [] = acc
        --
        succs current = S.toList $ S.map (\(x,y,z) -> (y,z)) (S.filter (\(x,y,z) -> x == current) trans)

firstActions :: TS -> Node -> Id -> Set Action -> Set Action
firstActions (states,init,events,trans) n0 id goal = traverse [n0] S.empty S.empty
  where 
    traverse (n:ns) visited current =
      if goal == current -- Once I have reached all the actions I could do, no need to look further
      then current
      else
        if S.member n visited 
        then traverse ns visited current 
        else let pairs = succs n
                 actions = -- record action id can do
                   S.map (\(Just e) -> e) $
                   S.filter isJust $ S.map (\(event, node) -> (project event id)) pairs
                 todo = -- take only those that reachable via an epsilon
                   S.map (\(x,y) -> y) $ 
                   S.filter (\(x,y) -> isNothing x ) $ S.map (\(event, node) -> ((project event id), node)) pairs
             in traverse
                (ns++(S.toList todo)) -- (S.toList $ S.union (S.fromList ns) (todo))
                (S.insert n visited)
                (S.union current actions)
    traverse [] visited current = current
        --
    succs current = S.map (\(x,y,z) -> (y,z)) (S.filter (\(x,y,z) -> x == current) trans)
        
  

--
-- Bowtie Relations
--
        
bowtieRel :: DiamondMap -> Set Event -> Bowtie
bowtieRel map set = S.fromList $ prod (S.toList set)
  where prod evts = 
          [(x,y) | x@(s1,s2,m1,m2,msg) <- evts, y@(s1',s2',m1',m2',msg') <- evts, 
           (x == y)
           ||
           (
             (msg == msg' && m1 == m1' && m2 == m2')
             &&
             (checkDiamond map m1 (s1, projectUnSafe x m1) (s1', projectUnSafe y m1'))
             &&
             (checkDiamond map m2 (s2, projectUnSafe x m2) (s2', projectUnSafe y m2'))
           )]
        

replaceEvt :: Bowtie -> Set Event -> Event -> Event
replaceEvt bowtie evtset e = findevt (S.toList evtset) e
  where findevt (x:xs) e = if S.member (x,e) bowtie
                           then x
                           else findevt xs e
        findevt [] e = e


-- --
-- -- Paths function(s)
-- --
-- simplePathsBetween :: (TS -> Node -> Set (Event, Node) )
--                       -> TS 
--                       -> Node -> Node
--                       -> Forest Event
-- simplePathsBetween f ts src target = findPath 0
--   where
--     size = transNumber ts
--     --
--     findPath max
--       | max > size = []
--       | otherwise = let (t,newpaths) = case dfs 0 max src [] of
--                           Just paths -> (True,paths)
--                           Nothing -> (False,[])
--                     in trace
--                        ("Trying: "++(show max)++" Bool: "++(show t)++"\nPaths: "++(show )
--                        (newpaths++findPath (max+1))
--     --
--     filterEdge visited current (label,node) = not $ L.elem (current,label,node) visited
--     --
--     dfs i max current visited 
--       | i > max = Nothing
--       | current == target = Just []
--       | otherwise = 
--         let newpairs = L.sortBy compareNode $ 
--                        L.nub $ L.map (\(x,y) -> (current,x,y)) $
--                        L.filter (\edge -> filterEdge visited current edge) (S.toList $ f ts current)
--             newforest = L.map (\(l,(Just f)) -> Node l f) $
--                         L.filter (isJust . snd) $
--                         L.map (\e@(n,l,n') -> (l,(dfs (i+1) max n' (e:visited)))) newpairs
--         in 
--          if L.null newforest
--          then Nothing 
--          else Just newforest
--     --
--     diff n n' = L.foldr (+) 0 $ L.map (\(x,y) -> if x==y then 0 else 1) (zip n n')
--     compareNode (n1,e1,n1') (n2,e2,n2') = compare
--                                          ((diff target n1') - (diff src n1'))
--                                          ((diff target n2') - (diff src n2'))



--
-- Printing functions
--
printNodeId :: Node -> String
printNodeId n = rmveQuotes $ L.foldl (++) "" n

        
printNodeLabel :: Node -> String
printNodeLabel n = rmveQuotes $ "("++(helper n)++")"
  where helper (x:y:xs) = (show x)++","++(helper (y:xs))
        helper [x] = (show x)
        helper [] = ""      
        
printEventLabel :: Event -> String
printEventLabel (q1, q2, m1, m2, msg) =
  rmveQuotes $ "("++(show q1)++","++(show q2)++","++(show m1)++"->"++(show m2)++":"++(show msg)++")"
        
printTS :: TS -> String
printTS (states, initstate, events, trans) =
  let header = "digraph ICTS { \n "
      footer = "}\n "
      nodes = S.map (\x -> (printNodeId x)++" [label=\""++(printNodeLabel x)++"\"]; \n ") states
      transi = S.map (\(s,e,t) -> (printNodeId s)++" -> "++(printNodeId t)++
                                 " [label=\""++(printEventLabel e)++"\"]; \n ") trans
  in header++(foldstring nodes)++(foldstring transi)++footer
     where foldstring s = S.fold (++) "" s
           
ts2file :: FilePath -> TS -> IO()
ts2file file ts = writeToFile (file++"_ts.dot") (printTS ts)


-- --
-- -- DEBUG
-- --
-- printAllPaths :: TS -> IO()
-- printAllPaths ts@(states, initstate, events, trans) = helper (S.toList states)
--   where helper (x:xs) = 
--           do
--             putStrLn $ "From "++(show initstate)++" to "++(show x)
--             putStrLn (show $ simplePathsPairs tssuccsPair ts initstate x)
--             helper xs
--         helper [] = putStrLn "all paths done.\n"
  