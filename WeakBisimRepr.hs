module WeakBisimRepr where

import TS
import CFSM
import Data.List as L
import qualified Data.Set as S
import Data.Map as M


-- CFSMs States = String
-- TS nodes = [String]
-- CFSMs label = Action = (Dir, Channel, Message)

ts2cfsm :: TS -> Id -> CFSM
ts2cfsm ts@(nodes, initnode, events, trans) id = (states, printNodeId initnode, actions, ntrans)
  where ntrans = S.map (\(n1,t,n2) -> (printNodeId n1, mproject t, printNodeId n2)) trans
        mproject t = case project t id of
          Nothing -> (Tau, (0,0), "")
          Just e -> e
        states = S.map printNodeId nodes
        actions = S.map (\(_,y,_) -> y) ntrans


act2lab :: Action -> String
act2lab (dir, chan, msg) = case dir of
  Send -> "act_"++(pchan chan)++"SEND"++msg
  Receive -> "act_"++(pchan chan)++"RECEIVE"++msg
  Tau -> "tau"
  where
    pchan :: (Id, Id) -> String
    pchan (i1,i2) = (show i1)++(show i2)

cfsm2fsm :: CFSM -> String
cfsm2fsm (states, initn, actions, trans) =
  let envi = (M.fromList $ snd $  mapAccumL (\x y -> (x+1,(y,x))) 1 (S.toList states)) :: Map String Int
      liststates = intercalate "\n" $ nub $ (show $ envi!initn):(L.map show $ M.elems envi)
      listtrans = intercalate "\n" $
                  L.map (\(x,y,z) -> (show $ envi!x)
                                     ++" "++(show $ envi!z)++" \""++(act2lab y)++"\"") (S.toList trans)
  in "n(0)\n---\n"++liststates++"\n---\n"++listtrans++"\n"
