module BranchingProperty where

import CFSM
import TS
import Data.List as L
import Data.Set as S
import Data.Map as M
import Data.Foldable as F
import Data.Maybe
import Dependency
import Misc
import Control.Concurrent
import Control.Exception
import Data.Ord
import Data.Tree as T
import PartialOrderReduction

import System.IO.Unsafe
import Debug.Trace

type MapActions = Map Id (Set Action)
-- type MapPaths = Map Node (Forest Event)
type EvtPath = [[(Node, Event, Node)]]
type MapPaths = Map Node EvtPath


isCommuting :: TS -> Node -> Event -> Event -> Bool
isCommuting ts@(nodes,initnode,events,tstrans) n e1 e2 = 
  F.or $ S.map (\x -> S.member (n1,e2,x) tstrans && S.member (n2,e1,x) tstrans) inter
  where n1 = tssucc ts n e1
        n2 = tssucc ts n e2
        inter = S.intersection (tssuccs ts n1) (tssuccs ts n2)
        

--
-- We only check whether a node is less another if
-- when they can fire at least two same events
-- and if they are directly connected
--
fireTwo :: TS -> Node -> Node -> Bool
fireTwo ts@(nodes,initnode,events,tstrans) n1 n2 =
  (S.size $ S.intersection (tssuccEvent ts n1) (tssuccEvent ts n2)) > 1
  &&
  not (S.null $ S.filter (\(x,y,z) -> (x==n1 && z==n2)) tstrans)
            

--
-- Build the (non transitive version) of <-relation on nodes of TS
--
buildLessRel :: TS -> Set (Node, Node)
buildLessRel ts@(nodes,initnode,events,tstrans) =
    helper (S.singleton initnode) S.empty (S.singleton initnode)
    where
      helper border acc visited = 
          let newpairs = S.fold S.union S.empty $
                         S.map (\n -> (S.map (\x -> (n,x))
                                       (S.filter (\y -> not $ S.member y visited) (tssuccs ts n)))) border
              newnodes = S.map snd newpairs
          in if S.null border || (visited == nodes)
             then acc
             else helper newnodes (S.union acc newpairs) (S.union newnodes visited)
              
 
fire :: TS -> Node -> Event -> Bool
fire ts@(nodes,initnode,events,tstrans) n e = not $ S.null $ S.filter (\(x,y,z) -> x==n && y == e) tstrans

--
-- Returns true if a n is a "last node" that fires e1 and e2
--
isLastNodeRel :: TS -> Set (Node, Node) -> Node -> Event -> Event -> Bool
isLastNodeRel ts@(nodes,initnode,events,tstrans) rel n e1 e2 = (F.and $ S.map checkNode allsucc)
  where allsucc = tssuccs ts n
        checkNode n' = not ((fire ts n' e1)  && (fire ts n' e2) && (S.member (n,n') rel))
        

        
checkBranchingProperty :: System -> TS -> Bool
checkBranchingProperty sys ts@(nodes,initnode,events,tstrans) =
  checkNodes sys (ts) (buildLessRel ts) (S.toList nodes)


--
-- DEPRECATED
--
checkBranchPropConcurrent :: System -> TS -> IO Bool
checkBranchPropConcurrent sys ts@(nodes,initnode,events,tstrans) =
  return $ checkNodes sys ts (buildLessRel ts) (S.toList nodes)


--
-- Check the Branching property on all nodes and branches in TS
--
checkNodes :: System -> TS -> Set (Node, Node) -> [Node] -> Bool
checkNodes sys ts lessRelation (x:xs) = checkNode x && checkNodes sys ts lessRelation xs
    where
        checkNode n = let list = S.toList $ tssuccEvent ts n in checkBranches n (pairs list)
        --
        pairs xs = L.nubBy eqTest [(x,y) | x <- xs, y <- xs, x /= y]
        eqTest (x,y) (x',y') = ((x==y') && (y==x')) || ((x==x') && (y==y'))
        --
        checkBranches n ((e1,e2):xs) = 
          (checkBranch n e1 e2)
          &&
          (checkBranches n xs)
        checkBranches n [] = True
        --
        checkBranch n e1 e2 =
            let succ1 = tssucc ts n e1
                succ2 = tssucc ts n e2
                map1 = mapActions sys ts succ1 e1 [0..(length sys)-1]
                map2 = mapActions sys ts succ2 e2 [0..(length sys)-1]
            in
              (independent e1 e2) --    <-- if they are independent, then they commute!
              ||
              (isCommuting ts n e1 e2)
              ||
              (
                not (isLastNodeRel ts lessRelation n e1 e2)
                       ||
                       (
                         (uniqueSender n e1 map1 e2 map2)
                         &&
                          -- Good Choice
                          (checkParticipants sys ts [0..(length sys)-1] n e1 map1 e2 map2) 
                          &&
                           -- Dependency relations
                          (checkReceivers (sender e1) sys ts map1 map2 [0..(length sys)-1] n e1 e2)
                       )
              )
        --
        uniqueSender n e1 map1 e2 map2 =
          let succ1 = tssucc ts n e1
              succ2 = tssucc ts n e2
              senders = 
                L.map 
                (
                  \id -> let lt1 = map1 ! id
                             lt2 = map2 ! id
                         in
                          if compatibleActions lt1 lt2
                          then existSend $ S.union lt1 lt2
                          else False
                ) [0..(length sys)-1]
          in checkSenders 0 senders
           where checkSenders num (x:xs) = checkSenders (if x then num+1 else num) xs
                 checkSenders i [] = if
                   i == 1
                   then True
                   else False
                        -- error $ "No unique sender at node: "++(show n)++
                        -- " e1: "++(show e1)++" e2: "++(show e2)++
                        -- " ("++(show i)++" senders)"

        --
checkNodes sys ts _ [] = True


--
-- Computes all the first action of all machines from a given node
--
mapActions :: System -> TS -> Node -> Event -> [Id] -> MapActions
mapActions sys ts n' e ids = M.fromList $ L.map (\x -> (x, helper x)) ids
  where helper id = case (project e id) of -- n' is the target of e here
          Just a -> S.singleton a
          Nothing -> firstActions ts n' id (possibleActions sys id n')
          
--
-- Check condition (c) of the Branching Property
--
checkReceivers :: Id -> System -> TS ->
                  MapActions ->
                  MapActions ->
                  [Id] -> Node -> Event -> Event -> Bool
checkReceivers snd sys ts map1 map2 (p:ps) n e1 e2 =
  if (checkReceiver p n e1 e2) 
  then (checkReceivers snd sys ts  map1 map2 ps n e1 e2)
  else error $ ("[RCV-race-condition] Machine: "++(show p)++" Node: "++(show n)++" e1: "++(show e1)++" e2: "++(show e2))
  where
    succ1 = tssucc ts n e1
    succ2 = tssucc ts n e2
    checkReceiver id n e1 e2 = 
          let lt1 = map1 ! id
              lt2 = map2 ! id
          in if (not $ existSend $ S.union lt1 lt2) && (compatibleActions lt1 lt2) && (not $ sameSenders lt1 lt2)
             then
               (checkRaceInChoice snd p ts succ1 e1 lt1 lt2)
               &&
               (checkRaceInChoice snd p ts succ2 e2 lt2 lt1)
             else True
checkReceivers _ sys ts _ _ [] _ _ _ = True

checkRaceInChoice :: Id -> Id -> TS -> Node -> Event -> Set Action -> Set Action -> Bool  
checkRaceInChoice snd rcv ts n ei good bad =
  dependencyTS snd rcv ts n ei (S.map action2interaction good) (S.map action2interaction bad)

--
-- Check condition (a) of the Branching Property
--
checkParticipants :: System -> TS -> [Id] -> Node -> 
                     Event -> MapActions ->
                     Event -> MapActions ->
                      Bool
checkParticipants sys ts (p:ps) n e1 map1 e2 map2 =
    if (checkParticipant p n e1 e2) 
    then (checkParticipants sys ts ps n e1 map1 e2 map2)
    else False -- error $ ("[PTS] Machine: "++(show p)++" Node: "++(show n)++" e1: "++(show e1)++" e2: "++(show  e2))
    where
      succ1 = tssucc ts n e1
      succ2 = tssucc ts n e2
      checkParticipant id n e1 e2 = 
          let lt1 = map1 ! id
              lt2 = map2 ! id
          in 
           if (compatibleActions lt1 lt2)
           then True
           else 
             goodMergeNode ts id n succ1 e1 succ2 e2
checkParticipants sys ts [] _ _ _ _ _  = True
       
        
compatibleActions :: Set Action -> Set Action -> Bool
compatibleActions set1 set2 = 
  (S.null set1 && S.null set2)
  ||
  (
   ((not $ S.null set1) && (not $ S.null set2))
   &&
   (S.null $ S.intersection set1 set2)
  )
  
  
 
sameSenders :: Set Action -> Set Action -> Bool
sameSenders act1 act2 = (snds act1) == (snds act2)
  where snds acts = S.map (\(d,(s,r),msg) -> s) acts



goodMergeNode :: TS -> Id -> Node -> Node -> Event ->  Node -> Event -> Bool
goodMergeNode ts id n n1 e1 n2 e2 =
  not $ S.null $ S.intersection (setX n1 e1) (setX n2 e2)
  where setX node evt = case project evt id of
          Just _ -> S.singleton n
          Nothing -> epsilonReachable ts id node
          
  
                        
                                   --
-- Find all the nodes reachable from another node
--  
epsilonReachable :: TS -> Id -> Node -> Set Node
epsilonReachable ts@(nodes,initnode,events,tstrans) id n  = 
  S.fromList $ traverse [n] S.empty [n]
  where
    traverse (n:ns) visited acc =
      if S.member n visited  
      then traverse ns visited acc
      else let sucnodes = S.toList $
                          S.map (\(l,s) -> s) $ 
                          S.filter (\(l,s) -> (isNothing $ project l id) && (not $ S.member s visited)) $ (tssuccsPair ts n)
           in traverse (ns++sucnodes) (S.insert n visited) (acc++sucnodes)
    traverse [] visited acc = acc   
         
 

--
-- Find all the nodes reachable from n1 and n2 (closest nodes first)
--
mergeNode :: TS -> Node -> Node -> [Node]
mergeNode ts n1 n2 = L.map fst intposition
  where reach1 = reachableNode ts n1
        reach2 = reachableNode ts n2
        --
        intsection = S.intersection (S.fromList reach1) (S.fromList reach2)
        --
        intposition = 
          L.sortBy ncompare $ S.toList $
          S.map (\x -> (x, (findPosition x reach1)+(findPosition x reach2))) intsection
        --
        ncompare (n,i) (n',j)
          | i < j = LT
          | i > j = GT
          | otherwise = EQ
        --
        findPosition n xs = case elemIndex n xs of
          Just i -> i
          Nothing -> error "Something went terribly wrong in finding a merging node!"
          
          
