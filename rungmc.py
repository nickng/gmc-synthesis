#!/usr/bin/python
import sys
import subprocess
import os
import os.path
import string
import time
import glob

# HASKELL TOOLS
GMC = "./GMC"
BG = "./BuildGlobal"

# EXTERNAL TOOLS
HKC = "./hknt-1.0/hkc"
PETRY = "/homes/jlange/Research/petrify/bin/petrify"
# DRAW = "/homes/jlange/Research/petrify/bin/draw_astg"

# WORKAROUND LACK OF escape character in PETRIFY
st_arrow = "AAA"
st_coma = "CCC"
st_colon = "COCO"

debug = False

factor = "0"

if len(sys.argv)>2:
    factor = sys.argv[2]

############################### START HERE ###################################
startTime = time.time()

#print "[PY] Executing ",GMC," ",sys.argv[1],factor,"..."
subprocess.check_call([GMC,sys.argv[1],factor,"+RTS","-N"])


mybasename = "outputs/"+os.path.basename(sys.argv[1])

with open('.machinenumber') as f:
    machine_number = int(f.readline())

if machine_number < 2:
    print "[PY] Fewer than two machines detected! I will terminate now."
    sys.exit()

prebool = True
for i in range(machine_number):
    #print "[PY] Testing machine "+str(i)+" for language equivalence"
    cmd = subprocess.Popen([HKC,"-sim","-equiv",mybasename+"_machine_"+str(i),mybasename+"_projection_"+str(i)], stdout=subprocess.PIPE)
    cmd.wait() # <---- Fix for Uni Cagliari (?)
    for line in cmd.stdout:
        spa = line.split('<<<')
        if len(spa) > 1:
            spb = spa[1].split('>>>')
            prebool = prebool and (spb[0].strip() == "true")
            print "[PY] Is machine "+str(i)+" representable? "+spb[0].strip()

print "Is the system Language-equivalence Representable (part (i))? ^^^"+str(prebool)+"^^^"
print ""


for i in range(machine_number):
    print "[PY] Testing machine "+str(i)+" for weak trace equivalence:"
    cmd = subprocess.Popen(["ltscompare","-eweak-trace",mybasename+"_lts_machine_"+str(i)+".fsm",mybasename+"_lts_projection_"+str(i)+".fsm"], stdout=subprocess.PIPE)
    cmd.wait() # <---- Fix for Uni Cagliari (?)
    for line in cmd.stdout:
        print line

for i in range(machine_number):
    print "[PY] Testing machine "+str(i)+" for weak bisim equivalence:"
    cmd = subprocess.Popen(["ltscompare","-esim",mybasename+"_lts_machine_"+str(i)+".fsm",mybasename+"_lts_projection_"+str(i)+".fsm"], stdout=subprocess.PIPE)
    cmd.wait() # <---- Fix for Uni Cagliari (?)
    for line in cmd.stdout:
        print line

subprocess.call(["dot","-Tsvg",mybasename+"_machines.dot","-o",mybasename+"_a_machines.svg"])

if debug:
    subprocess.call(["dot","-Tsvg",mybasename+"_ts.dot","-o",mybasename+"_b_ts.svg"])

# TS TO PETRIFY
#print "[PY] Calling petrify..."
subprocess.check_call([PETRY,"-dead","-ip","-efc",mybasename+"_toPetrify","-o","tempefc"])

replacements = [(st_coma,','), (st_arrow,'->'),(st_colon,':')]

# PETRIFY BACK TO HASKELL
with open(mybasename+"_petrinet", "wt") as fout:
    with open("tempefc", "rt") as fin:
        for line in fin:
            for (src, target) in replacements:
                line = line.replace(src,target)
            fout.write(line)
fout.close()
fin.close()

#print "[PY] Generating Global Graph..."
subprocess.check_call([BG,mybasename+"_petrinet"])
if debug:
    subprocess.check_call(["dot","-Tsvg",mybasename+"_petrinet_finalpn.dot","-o",mybasename+"_finalpn.svg"])
    subprocess.check_call(["dot","-Tsvg",mybasename+"_petrinet_preglobal.dot","-o",mybasename+"_preglobal.svg"], stderr=subprocess.PIPE)
subprocess.check_call(["dot","-Tsvg",mybasename+"_petrinet_global.dot","-o",mybasename+"_c_global.svg"], stderr=subprocess.PIPE)
#print "[PY] All Done."
########################################### ENDS HERE ############################################

endTime = time.time()

print ""
print "Execution time: ", endTime - startTime, "  ( Start:",str(startTime)," --> End:",str(endTime),")"



###################################### CLEANING UP #############################################

for fl in glob.glob("./"+mybasename+"_projection_*"):
    #print "Deleting: ",fl
    os.remove(fl)

for fl in glob.glob("./"+mybasename+"_machine_*"):
    #print "Deleting: ",fl
    os.remove(fl)

for fl in glob.glob("./"+mybasename+"*.dot"):
    #print "Deleting: ",fl
    os.remove(fl)

os.remove("./"+mybasename+"_petrinet")
os.remove("./"+mybasename+"_toPetrify")
