{-# LANGUAGE BangPatterns #-}
--
-- Julien Lange <j.lange@ic.ac.uk>
--
-- module Main where
import Control.Concurrent
import System.Environment
import System.FilePath.Posix
import CFSM
import TS
import Data.List as L
import Data.Map as M
import Data.Set as S
import HKCBridge
import Representability
import Misc
import BranchingProperty
import PetrifyBridge
import MultiplyCS
import PartialOrderReduction
import Text.Printf

projectionThread :: FilePath -> System -> TS -> MVar Bool -> IO ()
projectionThread filename system ts var = 
  do
    -- putStrLn "Projecting TS..."
    repLanguage filename system ts -- Machines and TS projections to timbuk files
    -- repLanguageConcurrent filename system ts -- /!\ Concurrent version takes more memory
    -- putStrLn "Translation to timbuk done."
    putMVar var True
     

representabilityThread :: System -> TS -> MVar Bool -> IO ()
representabilityThread system ts var = 
  do  -- putStrLn "Checking Representability -- part (ii)..."
      let repbra = (repBranching system ts)
      putMVar var repbra
      putStrLn $ "Is the system branching representable (part (ii))? ***"++(show repbra)++"***"
      
      
branchingPropertyThread ::  System -> TS -> MVar Bool -> IO ()
branchingPropertyThread system ts var = 
  do  -- putStrLn "Checking Branching Property..."
      let braprop = (checkBranchingProperty system ts)  
      -- braprop <- checkBranchPropConcurrent system ts -- <-- concurrent version (deprecated)
      putMVar var braprop
      putStrLn $ "Does the Branching Property holds? ***"++(show braprop)++"***"
      
      
printingThread :: FilePath -> System -> TS -> MVar Bool -> IO ()
printingThread filename system ts var = 
  do -- putStrLn "Printing Machines..."
     system2File filename system
     -- putStrLn "Converting TS to Petrify format..."
     writeToFile (filename++"_toPetrify") (ts2petrify ts)
     -- putStrLn "Printing done."
     putMVar var True

main :: IO ()
main =  do putStrLn "----------------- GMC Check -----------------"
           progargs <- getArgs
           case progargs of
             (x:xs) -> do
               let factor = if L.null xs then 0 else (read (head xs) :: Int)
               putStrLn $ "Parsing CFSMs file... (factor "++(show factor)++")"
               --------------------------------------------------
               cfsmfile <- readFile x
               let filename = "outputs/"++(takeFileName x)
               let system = 
                     multiply factor $ reverse $ parseSystem 0 (Prelude.map (\x -> words x) (lines cfsmfile)) emptyCFSM []
               --
               writeToFile ".machinenumber" (show $ length system)
               -- putStrLn $ "State numbers: "++(show $ stateNumbers system)++" ("++(show $ length system)++" machines)"
               -- putStrLn "Building TS..."
               --------------------------------------------------
               let ts =  buildTS system
               ts2file filename ts 
               -- putStrLn $ "Node number: "++(show $ nodeNumber ts)
               -- putStrLn $ "Transition number: "++(show $ transNumber ts)
               -- putStrLn $ "Density: "++(printf "%.4f" (density ts))
               -- putStrLn $ "Average Degree: "++(printf "%.4f" (averageDegree ts))
               --
               -- PARTIAL ORDER REDUCTION TESTS
               -- let reducedts = reduce ts
               -- putStrLn $ "[Reduced] Node number: "++(show $ nodeNumber reducedts)
               -- putStrLn $ "[Reduced] Transition number: "++(show $ transNumber reducedts)    
               -- writeToFile (filename++"_reduced_ts.dot") (printTS reducedts)
               --------------------------------------------------
               repbra <- newEmptyMVar
               braprop <- newEmptyMVar
               proj <- newEmptyMVar
               prnt <- newEmptyMVar
               --
               forkIO $ representabilityThread system ts repbra
               -- forkIO $ branchingPropertyThread system ts braprop
               forkIO $ projectionThread filename system ts proj
               forkIO $ printingThread filename system ts prnt
               branchingPropertyThread system ts braprop
               --
               v1 <- takeMVar repbra
               v2 <- takeMVar braprop
               putStrLn $ "<<----- Partial GMC Check: +++"++(show $ v1 && v2)++"+++ ----->> "
               --
               v3 <- takeMVar proj
               v4 <- takeMVar prnt
               --------------------------------------------------
               --
               if isEmpty ts 
                 then putStrLn "        /!\\  Warning: the TS appears to be empty, synthesis will fail!  /!\\"
                 else putStr ""
               if noSelfLoop ts 
                 then putStrLn "" -- "Partial GMC Check Done."
                 else putStrLn "        /!\\  Warning: the TS contains a self-loop, synthesis might fail!  /!\\"
             [] -> do 
               print "Error: enter a path to a CFSMs file"
