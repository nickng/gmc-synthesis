module HKCBridge where

import CFSM
import Misc
import Data.Char
import Data.Set as S
import Data.Char
import Data.List as L


--- HKC does not support numbers in symbols
channel2timbuk :: (Id,Id) -> String
channel2timbuk (m1,m2) = (int2timbuk m1)++(int2timbuk m2)

int2timbuk :: Int -> String
int2timbuk i = L.foldl (++) "" (convert (L.map digitToInt $ show i))
  where 
    convert (x:xs) = (single x):(convert xs)
    convert [] = []
    single x = case x of
      0 -> "zero"
      1 -> "one"
      2	-> "two"
      3	-> "three"
      4	-> "four"
      5	-> "five"
      6	-> "six"
      7	-> "seven"
      8	-> "eight"
      9	-> "nine"
      a -> error $ "ERROR:"++(show a)
  
state2timbuk :: State -> String
state2timbuk s = rmveQuotes (show s)

action2timbuk :: Action -> String
action2timbuk (dir, chan, msg) = rmveQuotes $
  case dir of
    Send -> (channel2timbuk chan)++"SEND"++(show msg)
    Receive -> (channel2timbuk chan)++"RECEIVE"++(show msg)

trans2timbuk :: LTrans -> String
trans2timbuk (q, action, q') =
  (action2timbuk action)++"("++(state2timbuk q)++")"++" -> "++(state2timbuk q')++"\n"

cfsm2timbuk :: CFSM -> Id -> String
cfsm2timbuk (states, initstate, actions, trans) id =
  let stactions = S.fold (++) "" (S.map (\x -> (action2timbuk x)++":1 ") actions)
      liststates = S.fold (++) "" (S.map (\x -> (state2timbuk x)++" ") states)
      header = "Ops xxxx:0 "++stactions++"\n \n"
      name = "Automaton "++(int2timbuk id)++"\n"
      ststates = "States "++liststates++"\n"
      finalstates = "Final States "++liststates++"\n"
      sttrans = "Transitions \n"++
                "xxxx() -> "++(state2timbuk initstate)++"\n"++
                S.fold (++) "" (S.map (\x -> (trans2timbuk x)) trans)
   in header++
      name++
      ststates++
      finalstates++
      sttrans
      
      